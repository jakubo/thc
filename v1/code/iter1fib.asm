; Find Fibonacci numbers using iterative method
; Max Fibonacci term that fits into 16 bit integer is 24 (46368)
; Fill memory cells 0-24 with their corresponding Fibonacci terms
; i.e: mem[0] = 0, mem[1] = 1 ... mem[24] = 46368

    MOVLW R10, 1        ; constant - 1;
    MOVLW R11, 25       ; max fibonacci term+1

    STORE R0, R0        ; store first term (0)
    STORE R10, R10      ; store second term (1)

    ADD R1, R10, R10    ; uint16_t term_addr = 2;
    ADD R2, R10, R0     ; uint16_t prev = 1;
    ADD R3, R0, R0      ; uint16_t prev_prev = 0;

ANOTHER:
    ADD R4, R2, R3      ; uint16_t curr = prev + prev_prev;
    STORE R4, R1        ; store current value
    ADD R3, R2, R0      ; prev_prev = prev
    ADD R2, R4, R0      ; prev = curr
    ADD R1, R1, R10     ; addr ++

    SUB R5, R11, R1     ; if last term
    JIZ STAY            ; stop and spin forever
    GOTO ANOTHER        ; else compute next

STAY: GOTO STAY
