; Find Fibonacci numbers using iterative method
; Max Fibonacci term that fits into 16 bit integer is 24 (46368)
; Fill memory cells 0-24 with their corresponding Fibonacci terms
; i.e: mem[0] = 0, mem[1] = 1 ... mem[24] = 46368
; Program takes 1924 clock cycles and then remains in STAY

    MOVLW   R11, 24     ; memory addr / fibonacci term

ANOTHER:
    ADD R1, R11, R0     ; Load FIB parameter
    CALL FIB
    STORE R2, R11       ; Store result in corresponding mem loc
    DECSZ R11           ; Check another one
    GOTO ANOTHER

STAY: GOTO STAY


FIB:
    ; r2 fib(r1)
    ; Returns r1'th fibonacci term (must be >= 0)
    ; Returned value is in R2
    ; Other used registers: R10, R3, R4

    MOVLW R10, 1        ; one (const)

    ADD R1, R1, R0
    JIZ RET0            ; handle input == 0
    
    SUB R1, R1, R10
    JIZ RET1            ; handle input == 1

    ADD R2, R10, R0     ; uint16_t next = 1;
    ADD R3, R10, R0     ; uint16_t prev = 1;
    ADD R4, R10, R0     ; uint16_t curr = 1;

FIBLOOP:                ; next=curr+prev; prev=curr; curr=next;
    SUB R1, R1, R10
    JIZ FIBDONE
    ADD R2, R3, R4
    ADD R3, R4, R0
    ADD R4, R2, R0
    GOTO FIBLOOP

RET0:
    ADD R2, R0, R0
    GOTO FIBDONE
RET1:
    ADD R2, R10, R0
    GOTO FIBDONE        ; Redundant, I know.
FIBDONE:
    RET
