; Create string containing ASCII characters 0x20 through 0x7E using
; naive method - each character is added by going through whole
; string and searching for NULL

    MOVLW R11, 1        ; const uint16_t one = 1;
    MOVLW R8, 0x20      ; const uint16_t first_char = 0x20;
    MOVLW R9, 0x7E      ; const uint16_t last_char = 0x7E;
    SUB R10, R9, R8
    ADD R10, R10, R11

DO_IT_AGAIN:
    CALL APPEND_CHAR
    JIZ DO_IT_AGAIN

STAY: GOTO STAY

APPEND_CHAR:
    ; bool appendChar()
    ; go through memory starting at 0
    ; Find NULL and replace with expected character
    ; returns "done" (true of done, false if not)
    ADD R2, R0, R0  ; uint16_t addr = 0;
LOOP:
    LOAD R3, R2     ; uing16_t chr = str[addr]
    JIZ BREAK
    ADD R2, R2, R11 ; addr++
    GOTO LOOP       ; while(chr != 0)
BREAK:
    SUB R4, R2, R10 ;
    JIZ NOT_DONE 

    ADD R4, R2, R8  ; mem[addr] = addr + 0x20
    STORE R4, R2
    ADD R4, R4, R11
    STORE R4, R0    ; mem[addr+1] = NULL
    GOTO DONE

NOT_DONE:   ; return 1;
    ADD R1, R11, R0
    RET
DONE:       ; freturn 0
    STORE R8, R0
    ADD R1, R0, R0
    RET
