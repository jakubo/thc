; Generate C-style (null terminated) string containint ASCII chars 0x20 through 0x7E
; String starts in memory location 0
; The string is generated backwards - start with terminating null and proceed
; torwards first character

    MOVLW R1, 95
    MOVLW R2, 0x20

    STORE R0, R1    ; terminating NULL
    DECSZ R1        ; r1-- , R1 is not 1 0 so nothing else will happen
                    ; a "trick" to spare a step of using MOVL and SUB for decrement

FILL_CHAR:
    ADD R3, R1, R2
    STORE R3, R1
    DECSZ R1
    GOTO FILL_CHAR

    ADD R3, R1, R2  ; fill first char (at idx 0)
    STORE R3, R1

DONE: GOTO DONE
