; Find Fibonacci numbers using recursive method
; Max Fibonacci term that fits into 16 bit integer is 24 (46368)
; Fill memory cells 0-24 with their corresponding Fibonacci terms
; i.e: mem[0] = 0, mem[1] = 1 ... mem[24] = 46368
; Program takes 5 152 803 clock cycles and then remains in STAY
    MOVLW R11, 1    ; const uint16_t r11 = 1;
    MOVLW R8, 24    ; uint16_t r8 = 24; // memory addr / fibonacci term (running variable)

ANOTHER:
    ADD R1, R8, R0     ; Load FIB parameter
    CALL FIB
    STORE R2, R8       ; Store result in corresponding mem loc
    DECSZ R8           ; Check another one
    GOTO ANOTHER

STAY: GOTO STAY


FIB:; x fib(n)
    ; R1 - n (term to compute)
    ; R2 - x result
    ; R11 - 1 (constant, global)
    ; R12 - temp var

    ADD R12, R1, R0     ; Handle input == 0
    JIZ RET0

    SUB R12, R1, R11    ; Handle input == 1
    JIZ RET1

    ; Recursive part
    ADD R12, SP, R11
    STORE R1, R12       ;   save n
    SUB R1, R1, R11     ;   n-1
    CALL FIB
    ADD R12, SP, R11
    LOAD R1, R12        ;   restore n
    ADD R12, SP, R11
    ADD R12, R12, R11
    STORE R2, R12       ;   save result of fib(n-1)
    SUB R1, R1, R11
    SUB R1, R1, R11     ;   n - 2
    CALL FIB

    ADD R12, SP, R11
    ADD R12, R12, R11
    LOAD R12, R12       ; tmp = fib(n-1)
    ADD R2, R2, R12
    RET

RET0:
    ADD R2, R0, R0
    RET
RET1:
    ADD R2, R11, R0
    RET
