; Test function calls
; set R1 to non-zero value, call function that clears it
; and then spin forever
    MOVLW R1, 123
    CALL CLRR1
INF:
    GOTO INF


CLRR1:
    XOR R1, R1, R1
    RET
