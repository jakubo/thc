; conditional jump
GOTO START

TRAP: GOTO TRAP

START:
    MOVLL R1, 111

    SUB R1, R1, R1
    JIZ TRAP

GOTO START  ; Program should never get here
