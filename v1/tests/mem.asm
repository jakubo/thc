; Multiply function
; R1 = R2 * R3
; R2 and R3 are not affected
; state is saved (and then restored) to/from memory (first cell)

MOVLW R1, 3     ; some random value, to make sure it's overwritten
MOVLW R2, 5
MOVLW R3, 11

CALL MUL        ; after this completes, R1 = R2 * R3 (55)
                ; R2 and R3 should remain 5 and 11 respectively

CHILL: GOTO CHILL


MUL:
STORE R3, R0
ADD R1, R0, R0  ; clear R1 (could also XOR with self)

LOOP:
ADD R1, R1, R2
DECSZ R3
GOTO LOOP

LOAD R3, R0
RET
