; multiplication
; R1 = R2 * R3
; R2/R3 may be changed in the process
; When done - halt (spin on last instruction ad infinitum)
    MOVLW R2, 10
    MOVLW R3, 15
MULTIPLY:
    ADD R1, R1, R2
    DECSZ R3
    GOTO MULTIPLY
WAIT: GOTO WAIT
