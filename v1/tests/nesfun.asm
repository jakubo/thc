; test nested calls

CALL F1
SPIN: GOTO SPIN     ; program should stay here
                    ; R1, R2, R3, R4 should have values of 1, 2, 3, 4


F1:     ; set R1 to 1 and call F2 and F3
    MOVLL R1, 1
    CALL F2
    CALL F3
    RET  


F2:     ; set R2 to 2
    MOVLL R2, 2
    RET


F3:     ; set R3 to 3 and call F4
    MOVLL R3, 3
    CALL F4
    RET


F4:     ; set R4 to 4
    MOVLL R4, 4
    RET
