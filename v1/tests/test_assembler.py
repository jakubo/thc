"""Tests for assembler"""
from thc import assy


def test_instruction_number():
    """Test that there are exactly 16 instructions"""
    assert len(assy.INSTRUCTIONS) == 16


def test_movlw_simplest():
    """Test simples MOVLW macro"""
    line = 'MOVLW R5, 3409'
    data = assy.parse_line(1, line)
    one, two = assy.movlw(data)
    assert one['line'] == 'MOVLH R5, 13'
    assert two['line'] == 'MOVLL R5, 81'


def test_movlw_with_label():
    """Test MOVLW macro with label"""
    line = 'HERE: MOVLW R5, 3409'
    data = assy.parse_line(1, line)
    one, two = assy.movlw(data)
    assert one['line'] == 'HERE: MOVLH R5, 13'
    assert two['line'] == 'MOVLL R5, 81'
