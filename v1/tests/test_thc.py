"""Run some tests on THC"""
import os

import pytest

from thc.core import THC
from thc.assy import process_file


def _load_program(file_name):
    """Returns THC with loaded program from
    assembler file ``file_name``.
    """
    file_path = os.path.join(os.path.dirname(__file__), file_name)
    program = list(process_file(file_path))
    return THC(program)


def _spin_cpu(cpu, cycles):
    """Spin ``cpu`` for ``cycles`` cycles"""
    for x in range(cycles):
        cpu.tick()


def test_simple_multiplication():
    """Multiply R2(10) and R3(15)
    At the end of program R1 should be 150
    """
    cpu = _load_program('mul.asm')
    assert cpu.registers[1] == 0
    _spin_cpu(cpu, 100)
    assert cpu.registers[1] == 150


def test_simple_function():
    """Tests function calls.
    After two cycles R1 should have value of 123
    After couple more it should be 0.
    """
    cpu = _load_program('fun.asm')
    cpu.tick()
    cpu.tick()
    assert cpu.registers[1] == 123
    _spin_cpu(cpu, 100)
    assert cpu.registers[1] == 0


def test_mem_access():
    """Tests memory access.
    Program multiplies R2 and R3 without affecting them
    (they are changed during computation, but later restored).
    """
    cpu = _load_program('mem.asm')
    _spin_cpu(cpu, 6)
    assert cpu.registers[1] == 3
    assert cpu.registers[2] == 5
    assert cpu.registers[3] == 11
    _spin_cpu(cpu, 500)
    assert cpu.registers[1] == 55
    assert cpu.registers[2] == 5
    assert cpu.registers[3] == 11


def test_nested_functions():
    """Test nested functions"""
    cpu = _load_program('nesfun.asm')
    _spin_cpu(cpu, 100)
    assert cpu.registers[1] == 1
    assert cpu.registers[2] == 2
    assert cpu.registers[3] == 3
    assert cpu.registers[4] == 4


def test_conditional_jump():
    """Tests JIZ.
    R1 should be  111 after 2 cycles.
    Few cycles ater it should be 0
    """
    cpu = _load_program('jiz.asm')
    _spin_cpu(cpu, 2)
    assert cpu.registers[1] == 111
    _spin_cpu(cpu, 10)
    assert cpu.registers[1] == 0


def test_alu_functions():
    """Tests all ALU functions"""
    cpu = _load_program('alu.asm')
    r123 = lambda: (cpu.registers[1], cpu.registers[2], cpu.registers[3])
    expected = (
        (0, 0, 0), (10, 0, 0), (10, 100, 0),
        (20, 100, 0), (20, 100, 80), (0, 100, 80),
        (64, 100, 80), (64, 80, 80), (64, 80, 65471)
    )
    for state in expected:
        assert r123() == state
        cpu.tick()


def test_program_memory_read_only():
    """Tests that program memory cannot be overwritten"""
    program = [0, 1, 2, 3]
    cpu = THC(program)
    assert cpu.program_memory[:4] == program
    cpu.program_memory[0] = 15
    assert cpu.program_memory[:4] == program


def test_program_max_size():
    """Tests that program can only be 2**12 long"""
    program = [5 for x in range(10_000)]
    cpu = THC(program)
    assert len(cpu.program_memory) == 2**12


def test_data_max_size():
    """Tests that data memory is only be 2**16"""
    cpu = THC([1, 2, 3])
    cpu.data_memory[100_000] = 5
    assert len(cpu.data_memory) == 2**16


def test_registers():
    """Tests that cpu has only 16 registers,
    and R0 is read only - 0
    """
    cpu = THC([1, 2, 3])
    with pytest.raises(KeyError):
        reg100 = cpu.registers[100]

    cpu.registers[1] = 5
    assert cpu.registers[1] == 5

    cpu.registers[0] = 7
    assert cpu.registers[0] == 0


def test_notification():
    """Tests tick notificaton"""
    count = 0
    def _callback(sender):
        nonlocal count
        count += 1

    cpu = THC([])
    cpu.on_tick.add(_callback)
    for x in range(122):
        cpu.tick()

    assert count == 122
