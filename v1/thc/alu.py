"""ALU"""
from .utils import compl2


class ALU:
    """Performs arithmetic and logical operations"""
    OPERATIONS = {
        0b001: '_add',
        0b010: '_sub',
        0b011: '_xor',
        0b100: '_and',
        0b101: '_or',
        0b110: '_nand'
    }

    def __init__(self):
        self.zero = 0

    def __call__(self, opcode, op1, op2):
        operation = getattr(self, self.OPERATIONS[opcode])
        result = operation(op1, op2)
        self.zero = (result == 0)
        return result

    def _add(self, op1, op2):
        return op1 + op2

    def _sub(self, op1, op2):
        return op1 - op2

    def _xor(self, op1, op2):
        return op1 ^ op2

    def _and(self, op1, op2):
        return op1 & op2

    def _or(self, op1, op2):
        return op1 | op2

    def _nand(self, op1, op2):
        return compl2(~(op1 & op2), 16)
