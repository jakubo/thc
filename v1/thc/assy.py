"""THC assembler"""
import re
from itertools import tee

from .core import THC


LINE_PATTERN = re.compile(
    r'^((?P<label>[A-Z0-9_]+):)?\s*(?P<instr>[A-Z]+)\s*(?P<params>.+)?$')


def instr(*args):
    """return callable that Take instruction
    arguments and returns them as 12 bit integer
    """
    def _instr(*operands):
        if len(operands) != len(args):
            raise ParseError(-1, -1)
        if not operands:
            return 0
        taken = 0
        machined = 0
        for arg, operand in zip(args, operands):
            shift = (12 - taken - arg)
            machined |= operand << shift
            taken += arg
        return machined
    return _instr


def movlw(data):
    """Full word literal MOV - expands into two MOVL's.
    Returns parsed native commands.
    """
    if not data['params'] or len(data['params']) != 2:
        raise ParseError(data['line_no'], data['line'])
    reg, value = data['params']

    msb = (to_int(value) & 0xFF00) >> 8
    lsb = to_int(value) & 0x00FF
    label = '{}: '.format(data['label']) if data['label'] else ''
    lno = data['line_no']
    yield parse_line(lno, '{}MOVLH {}, {}'.format(label, reg, msb))
    yield parse_line(lno, 'MOVLL {}, {}'.format(reg, lsb))


INSTRUCTIONS = {
    'MOVLL': instr(4, 8),
    'MOVLH': instr(4, 8),
    'LOAD': instr(4, 4),
    'STORE': instr(4, 4),
    'JIZ': instr(12,),
    'RET': instr(),
    'CALL': instr(12,),
    'GOTO': instr(12,),
    'DECSZ': instr(4,),
    'ADD': instr(4, 4, 4),
    'SUB': instr(4, 4, 4),
    'XOR': instr(4, 4, 4),
    'AND': instr(4, 4, 4),
    'OR': instr(4, 4, 4),
    'NAND': instr(4, 4, 4),
    'RETI': instr(),
}


MACROS = {
    'MOVLW': movlw
}


REGISTERS = {'R{}'.format(x): x for x in range(16)}
REGISTERS.update({'SP': 13, 'IR': 14, 'PC': 15})


class ParseError(ValueError):
    """Thrown when cannot figur out what to do with a line of assembly"""
    def __init__(self, line_no, line):
        super().__init__('Unable to parse line {}: {}'.format(line_no, line))


def parse_line(line_no, line):
    """Returns dictionary containing:
        instr,
        label (optional),
        prms (optional),
        line (original code line)
        line_no
    """
    match = LINE_PATTERN.match(line)
    try:
        data = match.groupdict()
    except AttributeError:
        raise ParseError(line_no, line)

    if data['params']:
        data['params'] = [x.strip() for x in data['params'].split(',')]
    else:
        data['params'] = []
    data['line'] = line
    data['line_no'] = line_no
    return data


def strip_comment(line):
    """Returns line up to ;"""
    return line.partition(';')[0]


def expand_macros(parsed_code):
    """Returns code with expanded macros"""
    for line in parsed_code:
        instr = line['instr']
        if instr in INSTRUCTIONS:
            yield line
        elif instr in MACROS:
            yield from MACROS[instr](line)
        else:
            raise ParseError(line['line_no'], line['line'])


def expand_labels(parsed_code):
    """Change labels to their numerical values.
    R0 - R16 are reserved for registers and will be overwritten if used
    """
    code = tee(parsed_code)
    labels = {x['label']: idx for (idx, x) in enumerate(code[0])}
    labels.update(REGISTERS)
    for line in code[1]:
        line['params'] = [labels.get(x, x) for x in line['params']]
        yield line


def expand_data_labels(parsed_code, data_labels):
    """Replaces data labels used in code with their numeric values.
    Example:
    parameters: [R1, VAR2]
    data_labels: {VAR2: 0x5, OTHER: 0xF
    result: [R1, 5]
    """
    for line in parsed_code:
        line['params'] = [data_labels.get(x, x) for x in line['params']]
        yield line


def to_int(val):
    """Convert ``val`` to integer.
    val needs to be base2, base10 or base16 number.
    Raise ValueError if not possible to convert.
    """
    try:
        return int(val)
    except ValueError:
        try:
            return int(val, 2)
        except ValueError:
            return int(val, 16)


def parse_params(parsed_code):
    """All parameters should be representing numbers at this point.
    Return base 10 integer representation.
    Allowed representations are: base 2, base 10, base 16.
    """
    for line in parsed_code:
        try:
            line['params'] = [to_int(x) for x in line['params']]
            yield line
        except ValueError:
            raise ParseError(line['line_no'], line['line'])


def generate_machine_code(parsed_code):
    """Generate machine code."""
    opcodes = THC.Instruction.__dict__
    for entry in parsed_code:
        opcode = entry['instr']
        machine_instr = opcodes[opcode] << 12
        machine_instr |= INSTRUCTIONS[opcode](*entry['params'])
        yield machine_instr


def process_file(asm_file):
    """Turns ``asm_file`` into bin file"""
    prog_addrs = {}
    with open(asm_file, 'rt') as asm:
        assembler = asm.read()
    
    # remove comments
    assembler = '\n'.join(strip_comment(x) for x in assembler.splitlines())
    # make labels inline, ensure there is exactly one space after the colon
    lines = re.sub(':[\s\n\r]+', ': ', assembler).splitlines()
    # Make everything uppercase, remove extra spaces, remove empty lines
    lines = (line.upper().strip() for line in lines if line.strip())

    parsed = (parse_line(lno, line) for (lno, line) in enumerate(lines))

    steps = (expand_macros, expand_labels, parse_params, generate_machine_code)

    for step in steps:
        parsed = step(parsed)

    return parsed
