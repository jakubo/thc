"""CPU core"""
from .alu import ALU
from .reg import Registers, Reg
from .memory import Memory


class THC:
    """Tolvu Hogun CPU"""

    class Instruction:
        """Instruction opcodes by name"""
        MOVLL = 0b0000
        MOVLH = 0b0001
        LOAD = 0b0010
        STORE = 0b0011
        JIZ = 0b0100
        RET = 0b0101
        CALL = 0b0110
        GOTO = 0b0111
        DECSZ = 0b1000
        ADD = 0b1001
        SUB = 0b1010
        XOR = 0b1011
        AND = 0b1100
        OR = 0b1101
        NAND = 0b1110
        RETI = 0b1111

    def __init__(self, program):
        """Intitalize CPU"""
        self.registers = Registers()
        self.program_memory = Memory(16, 2**12, program)
        self.data_memory = Memory(16, 2**16)
        self.zero = 0
        self.alu = ALU()
        self.tickcount = 0
        self.registers[Reg.SP] = 0xBBEF
        self.on_tick = set()

    def tick(self):
        """All the work is done here, on clock signal"""
        self.tickcount += 1
        self.fetch()
        self.execute()
        self._notify()

    def fetch(self):
        """Fetch next instruction into IR, increment PC"""
        instruction = self.program_memory[self.registers[Reg.PC]]
        self.registers[Reg.IR] = instruction
        self.registers[Reg.PC] += 1

    def execute(self):
        """Decode and execute instruction"""
        instruction = self.registers[Reg.IR]
        target = (instruction >> 8) & 0xF
        opcode = instruction >> 12
        is_mov = 0b0000 <= opcode <= 0b0001
        is_mem = 0b0010 <= opcode <= 0b0011
        is_branch = 0b0100 <= opcode <= 0b1000
        is_alu = 0b1001 <= opcode <= 0b1110
        if is_mov:
            high = opcode & 1
            val = instruction & 0xFF
            current_val = self.registers[target]
            if high:
                new_val = (val << 8) & (current_val & 0xFFFF)
            else:
                new_val = (0xFFFF & current_val) | val
            self.registers[target] = new_val
            self.zero = (new_val == 0)

        elif is_mem:
            addr = self.registers[(instruction >> 4) & 0xF]
            if opcode == self.Instruction.STORE:
                self.data_memory[addr] = self.registers[target]
            elif opcode == self.Instruction.LOAD:
                self.registers[target] = self.data_memory[addr]
                self.zero = (self.registers[target] == 0)

        elif is_branch:
            addr = instruction & 0xFFF
            if opcode == self.Instruction.GOTO:
                self.registers[Reg.PC] = addr
            elif opcode == self.Instruction.JIZ:
                if self.zero:
                    self.registers[Reg.PC] = addr
            elif opcode == self.Instruction.CALL:
                return_addr = self.registers[Reg.PC]
                self.registers[Reg.SP] += 0x11
                self.data_memory[self.registers[Reg.SP]] = return_addr
                self.registers[Reg.PC] = addr
            elif opcode == self.Instruction.RET:
                return_addr = self.data_memory[self.registers[Reg.SP]]
                self.registers[Reg.PC] = return_addr
                self.registers[Reg.SP] -= 0x11
            elif opcode == self.Instruction.DECSZ:
                self.registers[target] -= 1
                if self.registers[target] == 0:
                    self.registers[Reg.PC] += 1
            
        elif is_alu:
            operation = opcode & 0b111
            op1 = self.registers[(instruction >> 4) & 0xF]
            op2 = self.registers[instruction & 0xF]
            self.registers[target] = self.alu(operation, op1, op2)
            self.zero = self.alu.zero

    def _notify(self):
        """Send notification to all observers.
        This is not part of CPU per se, it is
        to facilitate emulator UI
        """
        for alert in self.on_tick:
            alert(self)
