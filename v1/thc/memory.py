"""Memory"""
from .utils import nbit


class Memory(list):
    """Memory"""
    def __init__(self, word_size, word_num, read_only_content=None):
        super().__init__()
        self.word_num = word_num
        self.word_size = word_size
        self.extend(0 for x in range(word_num))
        self.read_only = False
        if read_only_content:
            self[:len(read_only_content)] = read_only_content
            self.read_only = True

    def __setitem__(self, idx, val):
        if self.read_only:
            return
        if isinstance(idx, slice):
            if idx.stop >= self.word_num:
                return
            return super().__setitem__(idx, val)
        if idx >= self.word_num:
            return
        return super().__setitem__(idx, nbit(val, self.word_size))
