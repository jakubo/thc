"""Registers"""
from .utils import nbit


class Reg:
    """Names for registers"""
    R0 = 0
    R1 = 1
    R2 = 2
    R3 = 3
    R4 = 4
    R5 = 5
    R6 = 6
    R7 = 7
    R8 = 8
    R9 = 9
    R10 = 10
    R11 = 11
    R12 = 12
    R13 = 13
    R14 = 14
    R15 = 15
    SP = 13
    IR = 14
    PC = 15


class Registers(list):
    """Registers"""
    def __init__(self):
        super().__init__()
        self.extend(0 for x in range(16))

    def __getitem__(self, reg):
        if reg == Reg.R0:
            return 0
        if reg > Reg.R15:
            raise KeyError('There is only 16 registers')
        return super().__getitem__(reg)

    def __setitem__(self, reg, val):
        if reg == Reg.R0:
            return
        super().__setitem__(reg, nbit(val, 16))
