"""Misc utils/helpers"""


def nbit(value, bits):
    """Returns ``value`` trimmed to max ``bit`` bits"""
    mask = (2 ** bits) -1
    return value & mask


def compl2(val, bits):
    """Returns ``bits`` long two's complement of a ``val``"""
    bnum = bin(val).partition('b')[-1].rjust(bits, '0')
    inv = bnum.replace('1', '2').replace('0', '1').replace('2', '0')
    return int(inv, 2) + 1
