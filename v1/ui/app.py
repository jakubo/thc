"""Simple UI for THC"""
import os
import sys

from kivy.app import App
from kivy.lang import Builder
from kivy.uix.widget import Widget
from kivy.clock import Clock

from ui.port import Port
from thc.core import THC
from thc.assy import process_file


class THCUiApp(App):
    """Simple interactive THC UI"""
    def __init__(self, asm_file, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cpu = THC(list(process_file(asm_file)))

    def build(self):
        super().build()
        Clock.schedule_interval(lambda x: self.cpu.tick(), 0.01)
        self.cpu.on_tick.add(self._tick)

    def _tick(self, cpu):
        portr = self.root.ids['portr']
        portw = self.root.ids['portw']
        cpu.data_memory[0] = portr.port_data
        portw.port_data = cpu.data_memory[1]

        start = int(self.root.ids['mem_from'].text or 0)
        end = int(self.root.ids['mem_to'].text or 0)
        string = self.root.ids['as_string'].state == 'down'
        mem = cpu.data_memory[start:end]
        if string:
            mem_img = ''.join(chr(x) for x in mem)
        else:
            mem_img = str(mem)
        self.root.ids['mem'].text = mem_img

if __name__ == '__main__':
    pth = os.path.dirname(__file__)
    for fln in os.listdir(pth):
        if fln.endswith('.kv'):
            Builder.load_file(os.path.join(pth, fln))

    asm_file = sys.argv[1]
    thcui = THCUiApp(asm_file)
    thcui.run()
