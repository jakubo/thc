"""Port visualization"""
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import NumericProperty, BooleanProperty


class Port(BoxLayout):
    """16 bit port"""
    is_read_only = BooleanProperty(False)
    port_data = NumericProperty(0)
    _propagate = BooleanProperty(True)

    def on_port_data(self, *args, **kwargs):
        """Change "pins" to reflect data"""
        if self._propagate:
            self._data_to_pins()

    def pin_clicked(self, pin):
        if self.is_read_only:
            return
        pin.pin_state = not pin.pin_state
        self._propagate = False
        self._pins_to_data()
        self._propagate = True

    def _data_to_pins(self):
        """Update pins to reflect data"""
        try:
            data = bin(self.port_data & 0xFFFF)[2:].zfill(16)[::-1]
            for idx, bit in enumerate(data):
                self.ids['b'+str(idx)].pin_state = int(bit)
        except KeyError:
            pass

    def _pins_to_data(self):
        """Update port data from pins"""
        bits = [self.ids['b'+str(x)].pin_state for x in range(16)]
        self.port_data = int(''.join(str(int(x)) for x in bits[::-1]), 2)


class Pin(Button):
    """Pin"""
    pin_state = BooleanProperty(False)

    def on_pin_state(self, *args, **kwargs):
        color_on = [0.3, 1, 0.3, 1]
        color_off = [1, 1, 1, 1]
        self.background_color = color_on if self.pin_state else color_off
