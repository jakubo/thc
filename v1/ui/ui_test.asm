; Copy memory location 0 into memory location 1 infinitely

    MOVLL R1, 1     ; constant - 1

AGAIN:

    LOAD R2, R0
    STORE R2, R1

GOTO AGAIN
